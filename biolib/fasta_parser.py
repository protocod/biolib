from typing import Any, IO
from biolib.model.fasta import FastaSeq
import re


# partie 1 question 1 ParseFasta
class FastaParser:
    
    def get_pattern(self) -> str:
        return "(^(>.*\\n)(.*))*"


    def __is_valid_sequence(self, seq: str) -> bool:
        found = re.search("([ATCG]*)", seq).groups()

        if not(found and len(found) > 0):
            return False

        full_match = found[0]
        return len(full_match) == len(seq)


    def __parse_content(self, content: str) -> FastaSeq:
        found = re.search(self.get_pattern(), content, re.MULTILINE)
        if found and len(found.groups()) == 3:
            groups = found.groups()
            header = groups[1].replace('\n', '')
            seq = groups[2]

            if not self.__is_valid_sequence(seq):
                raise ValueError("Invalid fasta: bad sequence")

            return FastaSeq(
                header = header,
                seq = seq
            )
        else:
            raise ValueError("Invalid fasta: no sequence found")


    def parse(self, io: IO[Any]) -> FastaSeq:
        """
        Returns: a fasta sequence from an IO[Any] stream of fasta file
        """
        self.content = io.read()
        return self.__parse_content(self.content)