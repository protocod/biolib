from typing import Any, IO, Union, Dict, Tuple
from functools import reduce
from re import findall
from biolib.model.matrix import MatrixCost

# partie 1 question 2 ParseConfig
class ConfigParser:

    TOKENS = {
        'keywords': ('identity', 'substitution', 'indel'),
        'assignments': ['=']
    }


    def get_pattern(self) -> str:
        reducer = lambda acc, word: f'{acc}|{word}' if acc != '' else word
        keywords = reduce(reducer, ConfigParser.TOKENS['keywords'], '')
        assignments = reduce(reducer, ConfigParser.TOKENS['assignments'], '')
        return f'({keywords}){{1}}\\s*({assignments}){{1}}\\s*([-|+]{{0,1}}\\s*\\d+)\\s*'


    def __parse_content(self, content_read: str) -> Dict[str, str]:
        groups = findall(self.get_pattern(), content_read)

        def groups_to_dict(dict: Dict[str, str], group: Tuple[str, str, str]):
            keyword, _, value = group
            dict[keyword] = value
            return dict

        return reduce(groups_to_dict, groups, dict())


    def parse(self, io: IO[Any]) -> Union[MatrixCost, None]:
        """
        Returns: a MatrixCost object from an IO[Any] of score config
        """
        self.content_read = io.read()
        parsed_content = self.__parse_content(self.content_read)

        return MatrixCost(
            identity = float(parsed_content['identity']),
            substitution = float(parsed_content['substitution']),
            indel = float(parsed_content['indel'])
        )