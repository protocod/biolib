from typing import List, Tuple
from dataclasses import dataclass
from biolib.model.direction import Direction


@dataclass(frozen = True)
class MatrixCost:
    match: int
    mismatch: int
    indel: int


DNA = str
BaseRules = List[Tuple[str, str]]
NeedlemanWunschPoint = Tuple[int, Direction]
NeedlemanWunschMatrix = List[List[NeedlemanWunschPoint]]


def complementary(dna: DNA) -> DNA:
    """
    Returns: a dna's complementary
    > complementary('ATCG')
    'TAGC'
    """
    baseRules = [ ('A', 'T'), ('C', 'G') ]
    def transform_base(base: str, rules: BaseRules) -> str:
        for baseRule in rules:
            if base == baseRule[0]: return baseRule[1]
            if base == baseRule[1]: return baseRule[0]
        return ''
    
    return ''.join(transform_base(base, baseRules) for base in dna)


def get_score_by_pair_base(first_base: str, second_base: str, score: MatrixCost) -> int:
    """
    Returns: match/mismatch score by pair base.

    > get_score_by_pair_base('A', 'A')
    1
    > get_score_by_pair_base('B', 'A')
    -1
    > get_score_by_pair_base('-', 'A')
    -1
    """
    if first_base == '-' or second_base == '-': return score.indel #indel
    # match or mismatch
    return score.match if first_base == second_base else score.mismatch


def get_cell_value(score_matrix: NeedlemanWunschMatrix, i: int, j: int, first_base: str, second_base: str, score: MatrixCost) -> NeedlemanWunschPoint:
    top_pos = (i-1, j)
    left_pos = (i, j-1)
    top_left_pos = (i-1, j-1)
    
    computed_score = get_score_by_pair_base(first_base, second_base, score)

    score_from_diag = score_matrix[top_left_pos[0]][top_left_pos[1]][0] + computed_score
    score_from_top = score_matrix[top_pos[0]][top_pos[1]][0] + score.indel # indel
    score_from_left = score_matrix[left_pos[0]][left_pos[1]][0] + score.indel #indel

    computed_values = [score_from_diag, score_from_top, score_from_left]
    max_index = computed_values.index(max(computed_values))

    return (computed_values[max_index], Direction(max_index))


def get_needleman_wunsch_matrix(first_seq: DNA, second_seq: DNA, score: MatrixCost) -> NeedlemanWunschMatrix:
    """
    Returns: a scoring matrix used for lobal alignement algorithm.
    """
    first_seq = '-' + first_seq
    second_seq = '-' + second_seq
    score_matrix: NeedlemanWunschMatrix = [[(0, Direction.NONE) for _ in range(len(second_seq))] for _ in range(len(first_seq))]
    
    for i in range(1, len(first_seq)):
        score_matrix[i][0] = (i * score.indel, Direction.BOTTOM)

    for i in range(1, len(second_seq)):
        score_matrix[0][i] = (i * score.indel, Direction.RIGHT)

    for i in range(1, len(first_seq)):
        for j in range(1, len(second_seq)):
            score_matrix[i][j] = get_cell_value(score_matrix, i, j, first_seq[i], second_seq[j], score)
        
    return score_matrix


def get_alignment_path(score_matrix: NeedlemanWunschMatrix, first_seq: DNA, second_seq: DNA) -> List[Tuple[int, int]]:
    """
    Returns: coordonates which represent the alignment path
    """
    alignment_path = []
    first_seq = '-' + first_seq
    second_seq = '-' + second_seq

    i = len(first_seq) - 1
    j = len(second_seq) - 1

    def get_next_cell_pos(i: int, j: int, direction: Direction) -> Tuple[int, int]:
        if direction == Direction.BOTTOM: return (i - 1, j)
        if direction == Direction.RIGHT: return (i, j - 1)
        if direction == Direction.DIAG: return (i - 1, j - 1)
        return -1, -1

    while i >= 0 and j >= 0:
        cell = score_matrix[i][j]
        direction = cell[1]
        alignment_path.append([i, j])
        next_i, next_j = get_next_cell_pos(i, j, direction)
        i, j = next_i, next_j
        pass
    
    return alignment_path


def align_seq_seq(score_matrix: NeedlemanWunschMatrix, first_seq: DNA, second_seq: DNA, matrix_score: MatrixCost) -> Tuple[str, str]:
    """
    Returns: aligned sequences using needleman wunsch algorithm
    """
    alignment_first_seq = ''
    alignment_second_seq = ''
    i = len(first_seq)
    j = len(second_seq)

    while i > 0 or j > 0:
        if i > 0 and j > 0 and score_matrix[i][j][0] == score_matrix[i-1][j-1][0] + get_score_by_pair_base(first_seq[i-1], second_seq[j-1], matrix_score):
            alignment_first_seq = first_seq[i-1] + alignment_first_seq
            alignment_second_seq = second_seq[j-1] + alignment_second_seq
            i -= 1
            j -= 1
        elif i > 0 and score_matrix[i][j][0] == score_matrix[i-1][j][0] - 1:
            alignment_first_seq = first_seq[i-1] + alignment_first_seq
            alignment_second_seq = '-' + alignment_second_seq
            i -= 1
        else:
            alignment_first_seq = '-' + alignment_first_seq
            alignment_second_seq = second_seq[j-1] + alignment_second_seq
            j -= 1

    return (alignment_first_seq, alignment_second_seq)


def needleman_wunsch_matrix_to_str(matrix: NeedlemanWunschMatrix) -> str:
    """
    Returns: the needleman wunsch scoring matrix in string format
    """
    read_row = lambda row: ', '.join('{} {}'.format(point[0], point[1].name) for point in row)
    return ''.join('{}\n'.format(read_row(row)) for row in matrix)