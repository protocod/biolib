from biolib.cost import Cost
from biolib.model.matrix import MatrixScore, MatrixCell, Position2D
from biolib.model.alignment import LocalAlignmentResult
from biolib.model.direction import Direction
from biolib.model.fasta import FastaSeq
from typing import Tuple, List


class DPmatrix:

    def __init__(self, U: str, V: str, cost: Cost) -> None:
        """
        DPMatrix class represent the scoring matrix generated for local alignment.
        > use data attribute to get computed scoring_matrix
        """
        self.__first_seq = f'-{U}'
        self.__second_seq= f'-{V}'
        self.cost = cost
        self.__max_score_position = (0, 0)
        self.data = self.__build_matrix()


    def __get_cell_value(self, matrix_score: MatrixScore, i: int, j: int) -> MatrixCell:
        first_base = self.__first_seq[i]
        second_base = self.__second_seq[j]
        
        top_pos = (i-1, j)
        left_pos = (i, j-1)
        top_left_pos = (i-1, j-1)
        
        computed_score = self.cost(first_base, second_base)

        get_positive_or_null = lambda x: 0 if x < 0 else x

        score_from_diag = get_positive_or_null(matrix_score[top_left_pos[0]][top_left_pos[1]][0] + computed_score)
        score_from_top = get_positive_or_null(matrix_score[top_pos[0]][top_pos[1]][0] + self.cost.score.indel)
        score_from_left = get_positive_or_null(matrix_score[left_pos[0]][left_pos[1]][0] + self.cost.score.indel)

        computed_values = [0, score_from_diag, score_from_top, score_from_left]
        max_value = max(computed_values)
        max_index = computed_values.index(max_value)
        return (computed_values[max_index], Direction(max_index-1))


    def __build_matrix(self) -> MatrixScore:
        matrix_score: MatrixScore = [[(0, Direction.NONE) for _ in range(len(self.__second_seq))] for _ in range(len(self.__first_seq))]

        for i in range(1, len(self.__first_seq)):
            for j in range(1, len(self.__second_seq)):
                matrix_score[i][j] = self.__get_cell_value(matrix_score, i, j)

                x, y = self.__max_score_position

                if matrix_score[i][j] > matrix_score[x][y]:
                    self.__max_score_position = (i, j)

        return matrix_score

    
    def get_first_seq(self) -> str:
        """
        Returns: the first sequence.
        """        
        return self.__first_seq[1:]

    
    def get_second_seq(self) -> str:
        """
        Returns: the second sequence.
        """    
        return self.__second_seq[1:]


    def get_max_local_position(self) -> Position2D:
        """
        Returns: the maximum local position.
        """    
        return self.__max_score_position


    def get_max_local_value(self) -> float:
        """
        Returns: the maximum local value.
        """    
        x, y = self.__max_score_position
        return self.data[x][y][0]


class SmithWaterman:

    def __init__(self, U: FastaSeq, V: FastaSeq, cost: Cost) -> None:
        """
        Returns: a local aligner which use smith waterman algorithm to align U and V sequences using specific costs.
        """
        self.__cost = cost
        self.__first_fasta_seq = U
        self.__second_fasta_seq = V
        self.__dp_matrix = DPmatrix(U.seq, V.seq, cost)
        self.__first_seq_aligned = str()
        self.__second_seq_aligned = str()
        self.__alignment_path: List[Tuple[int, int]]= []
        self.__traceback()


    def __get_next_direction(self, position2d: Position2D) -> Direction:
        x, y = position2d
        
        top_left = self.__dp_matrix.data[x-1][y-1][0] + self.__cost(self.__dp_matrix.get_first_seq()[x-1], self.__dp_matrix.get_second_seq()[y-1])
        top   = self.__dp_matrix.data[x-1][y][0] + self.__cost.score.indel
        left = self.__dp_matrix.data[x][y-1][0]  + self.__cost.score.indel
        current = self.__dp_matrix.data[x][y]

        if current[0] == 0: return Direction.NONE

        cells = [top_left, top, left]
        max_value = max(cells)
        max_index = cells.index(max_value)

        return Direction(max_index)

    def __build_seq_foreach_base(
        self, 
        direction: Direction, 
        position2d: Position2D) -> Tuple[int, int]:
        x, y = position2d

        if direction == Direction.DIAG:
            self.__first_seq_aligned = f'{self.__dp_matrix.get_first_seq()[x-1]}{self.__first_seq_aligned}'
            self.__second_seq_aligned = f'{self.__dp_matrix.get_second_seq()[y-1]}{self.__second_seq_aligned}'
            return (x-1, y-1)

        elif direction == Direction.RIGHT:
            self.__first_seq_aligned = f'-{self.__first_seq_aligned}'
            self.__second_seq_aligned = f'{self.__dp_matrix.get_second_seq()[y-1]}{self.__second_seq_aligned}' 
            return (x,y-1)

        
        elif direction == Direction.BOTTOM:
            self.__first_seq_aligned = f'{self.__dp_matrix.get_first_seq()[x-1]}{self.__first_seq_aligned}'
            self.__second_seq_aligned = f'-{self.__second_seq_aligned}'
            return (x-1, y)

        return (x, y)


    def __traceback(self):        
        x, y = self.__dp_matrix.get_max_local_position()
        next_direction = self.__get_next_direction((x, y))

        while next_direction != Direction.NONE:
            x, y = self.__build_seq_foreach_base(next_direction, (x, y))
            self.__alignment_path.append((x, y))
            next_direction = self.__get_next_direction((x, y))


    def get_aligned_seq(self) -> Tuple[str, str]:
        """
        Returns: the couple of aligned sequences.
        """
        return (self.__first_seq_aligned, self.__second_seq_aligned)


    def get_alignment_result(self) -> LocalAlignmentResult:
        """
        Returns: the alignment result as an object.
        """
        return LocalAlignmentResult(
            cost = self.__cost.score,
            alignment_path = self.__alignment_path,
            sequences = (self.__first_fasta_seq, self.__second_fasta_seq),
            aligned_sequences = self.get_aligned_seq()
        )

    
    def __str__(self) -> str:
        """
        Returns: a AlignmentResult in string format/ 
        """
        return f'{self.get_alignment_result()}'