from biolib.model.fasta import FastaSeq
from dataclasses import dataclass
from typing import List, Tuple
from biolib.model.direction import Direction
from biolib.model.fasta import FastaSeq


@dataclass(frozen = True)
class MatrixCost:
    """Which is the score for a match"""
    identity: float
    
    """Which is the score for a mismatch"""
    substitution: float

    """Which is the score for a gaps"""
    indel: float


MatrixCell = Tuple[float, Direction]


MatrixScore = List[List[MatrixCell]]


Position2D = Tuple[int, int]