from dataclasses import dataclass


@dataclass(frozen = True)
class FastaSeq:

    """Fasta header string"""
    header: str
    
    """Fasta sequence string"""
    seq: str


    def __str__(self) -> str:
        return "{}\n{}".format(self.header, self.seq)