from enum import Enum


class Direction(Enum):
    
    NONE = -1
    
    DIAG = 0
    
    BOTTOM = 1
    
    RIGHT = 2