from dataclasses import dataclass
from functools import reduce
from typing import List, Tuple


# partie 2 question 8 DisplayDist ici il est question de sérialiser l'objet en string pour le print
@dataclass(frozen = True)
class SignificanceResult:

    initial_score: float

    number_of_sequences: int
    
    score_index: List[Tuple[float, float]]

    significance_score: float


    def __str__(self) -> str:
        """
        Returns: a SignificantResult in string format
        """
        def for_each_score_index(acc: str, current: Tuple[float, float]) -> str:
            score, number = current
            bar = '=' * int(number)
            is_initial_score = '*' if self.initial_score == score else ''
            return f'{acc}\n{is_initial_score}\t{score}\t{number}:{bar}'

        title = f'Significance of the score: {self.significance_score}% ({self.number_of_sequences} sequences)\n\n'
        header = '\tscore\t#'
        scores = reduce(for_each_score_index, self.score_index, '')

        return ''.join([title, header, scores])