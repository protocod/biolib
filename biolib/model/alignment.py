from dataclasses import dataclass
from typing import Tuple, List
from biolib.model.fasta import FastaSeq
from biolib.model.matrix import MatrixCost


@dataclass(frozen = True)
class AlignmentBounds:

    start_seq1: int

    start_seq2: int

    end_seq1: int

    end_seq2: int


class LocalAlignmentResult:

    def __init__(
        self, 
        cost: MatrixCost,
        alignment_path: List[Tuple[int, int]],
        sequences: Tuple[FastaSeq, FastaSeq], 
        aligned_sequences: Tuple[str, str]) -> None:
        self.__cost = cost
        self.__sequences = sequences
        self.__aligned_sequences = aligned_sequences
        self.__alignment_path = alignment_path
        self.__alignment = ''
        self.__number_of_matches = 0
        self.__number_of_gaps = 0
        self.__number_of_mismatches = 0
        
        first_aligned_seq, second_aligned_seq = self.__aligned_sequences
        self.__alignment = str()

        for base_from_seq1, base_from_seq2 in zip(first_aligned_seq, second_aligned_seq):
            if base_from_seq1 == base_from_seq2:
                self.__alignment = f'{self.__alignment}|'
                self.__number_of_matches += 1
            
            elif base_from_seq1 == '-' or base_from_seq2 == '-':
                self.__alignment = f'{self.__alignment} '
                self.__number_of_gaps += 1
            
            else:
                self.__alignment = f'{self.__alignment}:'
                self.__number_of_mismatches += 1

    
    def get_cost(self) -> MatrixCost:
        return self.__cost


    def get_sequences(self) -> Tuple[FastaSeq, FastaSeq]:
        return self.__sequences


    def get_aligned_sequences(self) -> Tuple[str, str]:
        return self.__aligned_sequences


    def get_number_of_matches(self) -> int:
        return self.__number_of_matches


    def get_number_of_gaps(self) -> int:
        return self.__number_of_gaps


    def get_number_of_mismatches(self) -> int:
        return self.__number_of_mismatches


    def get_matches_score(self) -> float:
        return self.__number_of_matches * self.__cost.identity 

    
    def get_mismatches_score(self) -> float:
        return self.__number_of_mismatches * self.__cost.substitution

    
    def get_gaps_score(self) -> float:
        return self.__number_of_gaps * self.__cost.indel


    def get_similarity_score(self) -> float:
        return self.get_matches_score() + self.get_gaps_score() + self.get_mismatches_score()


    def get_alignment_bounds(self) -> AlignmentBounds:
        start_seq1, start_seq2 = self.__alignment_path[-1]
        end_seq1, end_seq2 = self.__alignment_path[0]
        return AlignmentBounds(
            start_seq1 = start_seq1 + 1,
            start_seq2 = start_seq2 + 1,
            end_seq1 = end_seq1 + 1,
            end_seq2 = end_seq2 + 1
        )


    def __str__(self) -> str:
        first_fasta_seq, second_fasta_seq = self.__sequences
        first_aligned_seq, second_aligned_seq = self.__aligned_sequences

        alignments_bounds = self.get_alignment_bounds()

        header = f'{first_fasta_seq}\n{second_fasta_seq}\n\nAlignment score: {self.get_similarity_score()}\n'

        first_fasta_seq_header = first_fasta_seq.header.split(' ')[0]
        second_fasta_seq_header = second_fasta_seq.header.split(' ')[0]

        return f'{header}\n{first_fasta_seq_header[:5]}\t {alignments_bounds.start_seq1} {first_aligned_seq} {alignments_bounds.end_seq1}\n\t   {self.__alignment}\n{second_fasta_seq_header[:5]}\t {alignments_bounds.start_seq2} {second_aligned_seq} {alignments_bounds.end_seq2}'