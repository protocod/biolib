from biolib.cost import Cost
from biolib.model.fasta import FastaSeq
from biolib.model.matrix import MatrixCost
from biolib.model.significance import SignificanceResult
from biolib.aligner.local_aligner import SmithWaterman
import random


# partie 2 question 7 Significance
def significance(
    V: FastaSeq, 
    U: FastaSeq, 
    initial_score: float, 
    number_of_sequences: int, 
    cost: MatrixCost) -> SignificanceResult:
    """
    Returns: the significance score of 2 sequences using an initial score, a number of random sequences and specific
    """
    random_scores = []
    superior_score_counter = 0
    score_index = dict()

    for i in range(number_of_sequences):
        U_list = list(U.seq)
        random.shuffle(U_list)
        S_seq = ''.join(U_list)
        aligner = SmithWaterman(
            FastaSeq(header = f'random sequence {i+1}', seq = S_seq),
            V,
            Cost(cost)
        )
        alignment_result = aligner.get_alignment_result()
        similarity_score = alignment_result.get_similarity_score()
        random_scores.append(similarity_score)

        if similarity_score > initial_score:
            superior_score_counter += 1

        if similarity_score in score_index:
            score_index[similarity_score] += 1
        
        else:
            score_index[similarity_score] = 1

    sorted_key = sorted(score_index)
    sorted_dict = list(map(lambda key: (key, score_index[key]), sorted_key))

    return SignificanceResult(
        initial_score = initial_score,
        number_of_sequences = number_of_sequences,
        score_index = sorted_dict,
        significance_score = (superior_score_counter / number_of_sequences ) * 100
    )
