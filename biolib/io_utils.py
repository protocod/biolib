from typing import Any, IO, Callable, Union


IOCallback = Callable[[IO[Any]], Any]    


def open_stream(source: Union[str, bytes, int], callback: IOCallback, mode: str = 'r') -> Any:
    """
    Open an IO[Any] stream from a source and manipulate the stream using a callback. You can use it to open a file safely.
    """
    with open(source, mode) as f:
        return callback(f)