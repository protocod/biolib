from biolib.model.significance import SignificanceResult
from unittest import TestCase
from biolib.significance import significance
from biolib.model.fasta import FastaSeq
from biolib.model.matrix import MatrixCost
import random


class SignificanceTest(TestCase):

    def test_significance(self):
        testing_data = [
            {
                'args': [
                    FastaSeq('>seq1', 'TGTTACGG'), 
                    FastaSeq('>seq2', 'GGTTGACTA'),
                    8,
                    200,
                    MatrixCost(
                        identity = 2,
                        substitution = -1,
                        indel = -2
                    )
                ],
                'expected': SignificanceResult(
                    initial_score = 8,
                    number_of_sequences = 200,
                    significance_score = 15.0,
                    score_index = [(4, 11), (5, 27), (6, 58), (7, 36), (8, 38), (9, 11), (10, 14), (11, 2), (12, 3)]
                )
            }
        ]

        random.seed(100)

        for data in testing_data:
            first_seq, second_seq, score, n, cost = data['args']
            significance_result = significance(first_seq, second_seq, score, n, cost)
            self.assertEqual(significance_result, data['expected'])