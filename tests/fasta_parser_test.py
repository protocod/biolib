from biolib.model.fasta import FastaSeq
from functools import reduce
from unittest import TestCase
from biolib.fasta_parser import FastaParser
from io import StringIO



class FastaParserTest(TestCase):

    def test_parse_mono_fasta_file(self):
        parser = FastaParser()

        testing_data = [
            {
                'args': StringIO(">TROLOLOLO HAGV01071440.1 TSA: Squalus acanthias, contig TR91722|c0_g2_i1, transcribed RNA sequence\nGACCCCCATCAAGCTGGGAGTGTCCCCCACAGCAGCACAGAGATGGCGGCTCACAGACAGACAGCAGACA"),
                'expected': FastaSeq(
                    header = ">TROLOLOLO HAGV01071440.1 TSA: Squalus acanthias, contig TR91722|c0_g2_i1, transcribed RNA sequence",
                    seq = "GACCCCCATCAAGCTGGGAGTGTCCCCCACAGCAGCACAGAGATGGCGGCTCACAGACAGACAGCAGACA"
                )
            }
        ]

        for data in testing_data:
            content = data['args']
            fasta_seq_parsed = parser.parse(content)
            self.assertEqual(fasta_seq_parsed, data['expected'])


    def test_parse_bad_mono_fasta_file(self):
        parser = FastaParser()

        testing_data = [
            {
                'args': StringIO(">seq_1 invalide\nTGTTACGG9ATGCGA"),
                'expected': ValueError
            }
        ]

        for data in testing_data:
            content = data['args']
            self.assertRaises(ValueError, lambda: parser.parse(content))