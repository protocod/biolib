from unittest import TestCase, main
from biolib.aligner.global_aligner import MatrixCost, complementary, get_alignment_path, get_score_by_pair_base, align_seq_seq, get_needleman_wunsch_matrix


class NeedlemanWunschTest(TestCase):

    def test_complementary(self):
        dna = 'ATCG'
        expected = 'TAGC'
        self.assertEqual(complementary(dna), expected)
    

    def test_get_score_by_pair_base(self):
        self.assertEqual(get_score_by_pair_base('A', 'A', MatrixCost(match = 1, mismatch = -1, indel = -1)), 1)
        self.assertEqual(get_score_by_pair_base('A', 'T', MatrixCost(match = 1, mismatch = -1, indel = -1)), -1)
        self.assertEqual(get_score_by_pair_base('-', 'A', MatrixCost(match = 1, mismatch = -1, indel = -1)), -1)
        self.assertEqual(get_score_by_pair_base('A', '-', MatrixCost(match = 1, mismatch = -1, indel = -1)), -1)
    

    def test_get_needleman_wunsch_matrix(self):
        matrix_score = MatrixCost(match = 1, mismatch = -1, indel = -1)
        testing_data = [
            {
                'computed': get_needleman_wunsch_matrix('TTC', 'ATT', matrix_score),
                'expected': [
                    [0, -1, -2, -3],
                    [-1, -1, 0, -1],
                    [-2, -2, 0, 1],
                    [-3, -3, -1, 0]
                ]
            },
            {
                'computed': get_needleman_wunsch_matrix('ATGGCAA', 'ATCGGAG', matrix_score),
                'expected': [
                    [0, -1, -2, -3, -4, -5, -6, -7],
                    [-1, 1, 0, -1, -2, -3, -4, -5],
                    [-2, 0, 2, 1, 0, -1, -2, -3],
                    [-3, -1, 1, 1, 2, 1, 0, -1],
                    [-4, -2, 0, 0, 2, 3, 2, 1],
                    [-5, -3, -1, 1, 1, 2, 2, 1],
                    [-6, -4, -2, 0, 0, 1, 3, 2],
                    [-7, -5, -3, -1, -1, 0, 2, 2],
                ]
            }                
        ]

        def extract_value_from_matrix(matrix):
            computed = []
            for i in range(len(matrix)):
                computed.append([])
                for j in range(len(matrix[i])):
                    computed[i].append(matrix[i][j][0])
            return computed
            
        for data in testing_data:
            computed = extract_value_from_matrix(data['computed'])
            self.assertEqual(computed, data['expected'])


    def test_get_alignement_path(self):
        matrix_score = MatrixCost(match = 1, mismatch = -1, indel = -1)
        testing_data = [
            {
                'dna': ('TTC', 'ATT'),
                'expected': [
                    [3, 3],
                    [2, 3],
                    [1, 2],
                    [0, 1],
                    [0, 0]
                ]
            },
            {
                'dna': ('ATGGCAA', 'ATCGGAG'),
                'expected': [
                    [7, 7],
                    [6, 6],
                    [5, 5],
                    [4, 5],
                    [3, 4],
                    [2, 3],
                    [2, 2],
                    [1, 1],
                    [0, 0]
                ]
            }
        ]

        for data in testing_data:
            first_seq, second_seq = data['dna']
            expected_alignement_path = data['expected']
            computed_wunsch_matrix = get_needleman_wunsch_matrix(first_seq, second_seq, matrix_score)
            computed_alignement_path = get_alignment_path(computed_wunsch_matrix, first_seq, second_seq)
            self.assertEqual(computed_alignement_path, expected_alignement_path)


    def test_align_seq_seq(self):
        matrix_score = MatrixCost(match = 1, mismatch = -1, indel = -1)
        testing_data = [
            {
                'dna': ('TTC', 'ATT'),
                'expected': ('-TTC', 'ATT-')
            },
            {
                'dna': ('ATGGCAA', 'ATCGGAG'),
                'expected': ('AT-GGCAA', 'ATCGG-AG')
            }
        ]

        for data in testing_data:
            first_seq, second_seq = data['dna']
            expected_first_seq, expected_second_seq = data['expected']
            computed_wunsch_matrix = get_needleman_wunsch_matrix(first_seq, second_seq, matrix_score)
            computed_first_seq, computed_second_seq = align_seq_seq(computed_wunsch_matrix, first_seq, second_seq, matrix_score)
            self.assertEqual(computed_first_seq, expected_first_seq)
            self.assertEqual(computed_second_seq, expected_second_seq)


if __name__ == '__main__':
    main()