from biolib.model.matrix import MatrixCost
from unittest import TestCase
from biolib.cost import Cost
from biolib.aligner.local_aligner import DPmatrix, SmithWaterman
from biolib.model.fasta import FastaSeq


class DPmatrixTest(TestCase):

    def test_DPmatrix(self):
        testing_data = [
            {
                'args': [
                    'GTGAATA', 
                    'ACCGTGA',  
                    Cost(
                        MatrixCost(
                            identity = 1,
                            substitution = -1,
                            indel = -1
                        )
                    )
                ],

                'expected': [
                    [0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 1, 0, 1, 0],
                    [0, 0, 0, 0, 0, 2, 1, 0],
                    [0, 0, 0, 0, 1, 1, 3, 2],
                    [0, 1, 0, 0, 0, 0, 2, 4],
                    [0, 1, 0, 0, 0, 0, 1, 3],
                    [0, 0, 0, 0, 0, 1, 0, 2],
                    [0, 1, 0, 0, 0, 0, 0, 1],
                ]
            },
            {
                'args': [
                    'TGTTACGG', 
                    'GGTTGACTA',  
                    Cost(
                        MatrixCost(
                            identity = 3,
                            substitution = -3,
                            indel = -2
                        )
                    )
                ],

                'expected': [
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 3, 3, 1, 0, 0, 3, 1],
                    [0, 3, 3, 1, 1, 6, 4, 2, 1, 0],
                    [0, 1, 1, 6, 4, 4, 3, 1, 5, 3],
                    [0, 0, 0, 4, 9, 7, 5, 3, 4, 2],
                    [0, 0, 0, 2, 7, 6, 10, 8, 6, 7],
                    [0, 0, 0, 0, 5, 4, 8, 13, 11, 9],
                    [0, 3, 3, 1, 3, 8, 6, 11, 10, 8],
                    [0, 3, 6, 4, 2, 6, 5, 9, 8, 7],
                ]
            }
        ]

        def matrix_values(matrix):
            computed = []
            for i in range(len(matrix)):
                computed.append([])
                for j in range(len(matrix[i])):
                    computed[i].append(matrix[i][j][0])
            return computed
        
        for data in testing_data:
            first_seq, second_seq, cost = data["args"]
            dp_matrix = DPmatrix(first_seq, second_seq, cost)
            computed_matrix_score = matrix_values(dp_matrix.data)
            self.assertEqual(computed_matrix_score, data["expected"], msg = "Matrix score should be equal")


class SmithWatermanTest(TestCase):

    def test_alignment(self):
        testing_data = [
            {
                'args': [
                    FastaSeq('>seq1', 'GTGAATA'), 
                    FastaSeq('>seq2', 'ACCGTGA'),
                    Cost(
                        MatrixCost(
                            identity = 1,
                            substitution = -1,
                            indel = -1
                        )
                    )
                ],
                'expected': ['GTGA', 'GTGA']
            },
            {
                'args': [
                    FastaSeq('>seq1', 'TGTTACGG'), 
                    FastaSeq('>seq2', 'GGTTGACTA'),
                    Cost(
                        MatrixCost(
                            identity = 3,
                            substitution = -3,
                            indel = -2
                        )
                    )
                ],
                'expected': ['GTT-AC', 'GTTGAC']
            },
            {
                'args': [
                    FastaSeq('>seq1', 'AGCATCGATCGACTGATCGAGCTAGCTACTGCTAGCTAGAG'), 
                    FastaSeq('>seq2', 'AGCATCGATCGACTGATCGAAAAAAAAAAAAGCTAGCTACTGCTAGCTAGAG'),
                    Cost(
                        MatrixCost(
                            identity = 2,
                            substitution = -1,
                            indel = -2
                        )
                    )
                ],
                'expected': [
                    'AGCATCGATCGACTGATCG-----------AGCTAGCTACTGCTAGCTAGAG',
                    'AGCATCGATCGACTGATCGAAAAAAAAAAAAGCTAGCTACTGCTAGCTAGAG'
                ]
            },
            {
                'args': [
                    FastaSeq('>seq1', 'GCTACGATCGATCTTACGGATCTGATCGTACGTGCCGTAGCTAGCTAGC'), 
                    FastaSeq('>seq2', 'AGCGGCTAGCTATGCTATATATAGTCTACGGCGGCTAGCTACGATCGTA'),
                    Cost(
                        MatrixCost(
                            identity = 2,
                            substitution = -1,
                            indel = -2
                        )
                    )
                ],
                'expected': [
                    'GCTA-CGAT-CGATCTTACGGATCTGATCGTACGTGCCGTAGCTA-GCTAG',
                    'GCTAGCTATGCTAT-ATATAG-TCT-A-CG-GCG-G-C-TAGCTACGATCG'
                ]
            },
            {
                'args': [
                    FastaSeq('>seq1', 'ACGGCCGATGCATGCCTAGCTAGTGTGAGTCGGGCAAG'), 
                    FastaSeq('>seq2', 'ACGGCCGATGCATGCCTAGCTAGTAAACCTGGTGATGCATGCTACAGC'),
                    Cost(
                        MatrixCost(
                            identity = 2,
                            substitution = -1,
                            indel = -2
                        )
                    )
                ],
                'expected': [
                    'ACGGCCGATGCATGCCTAGCTAGT----GTGAGTCGGGCAAG',
                    'ACGGCCGATGCATGCCTAGCTAGTAAACCTG-GTGATGCATG'
                ]
            }
        ]

        for data in testing_data:
            first_seq, second_seq, cost = data['args']
            first_seq_expected, second_seq_expected = data['expected']

            aligner = SmithWaterman(first_seq, second_seq, cost)            
            aligned_first_seq, aligned_second_seq = aligner.get_aligned_seq()

            self.assertEqual(aligned_first_seq, first_seq_expected)
            self.assertEqual(aligned_second_seq, second_seq_expected)
