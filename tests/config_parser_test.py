from unittest import TestCase
from biolib.config_parser import ConfigParser
from biolib.model.matrix import MatrixCost
from io import StringIO


class ConfigParserTest(TestCase):
    
    GOOD_CONFIG_EXAMPLES = [
        {
            'content': StringIO("identity=+2\nsubstitution=-1\nindel = -2"),
            'expected': MatrixCost(
                identity = 2,
                substitution = -1,
                indel = -2
            )
        },
        {
            'content': StringIO("identity=1\n\nindel = -4\tsubstitution = 1"),
            'expected': MatrixCost(
                identity = 1,
                substitution = 1,
                indel = -4
            )
        }
    ]

    BAD_CONFIG_EXAMPLES = [
        {
            'content': StringIO("identity=\nsubstitution=-1\nindel = -2"),
            'expected': KeyError
        }
    ]


    def test_get_pattern(self):
        parser = ConfigParser()
        pattern = parser.get_pattern()
        self.assertEqual(pattern, '(identity|substitution|indel){1}\\s*(=){1}\\s*([-|+]{0,1}\\s*\\d+)\\s*', 'These strings should be equals')

    
    def test_parse(self):
        parser = ConfigParser()

        for good_example in ConfigParserTest.GOOD_CONFIG_EXAMPLES:
            computed_config = parser.parse(good_example['content'])
            self.assertEqual(computed_config, good_example['expected'], 'Config data objects should be equals')

        for bad_example in ConfigParserTest.BAD_CONFIG_EXAMPLES:
            self.assertRaises(bad_example['expected'], lambda: parser.parse(bad_example['content']))