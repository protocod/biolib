from unittest import TestCase
from biolib.model.matrix import MatrixCost
from biolib.cost import Cost


class CostTest(TestCase):

    def test_cost_call(self):
        score = MatrixCost(
            identity = 2,
            substitution = -1,
            indel = -2
        )

        alphabet = 'ATCG-'
        cost = Cost(score, alphabet)
        
        self.assertEqual(cost('A', 'A'), score.identity)
        self.assertEqual(cost('A', 'T'), score.substitution)
        self.assertEqual(cost('A', '-'), score.indel)
        self.assertEqual(cost('T', 'A'), score.substitution)
        self.assertRaises(ValueError, lambda: cost('A', '9'))