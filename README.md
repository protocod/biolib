# Biotools-py

[![pipeline status](https://gitlab.com/protocod/biolib/badges/master/pipeline.svg)](https://gitlab.com/protocod/biolib/-/commits/master)
[![coverage report](https://gitlab.com/protocod/biolib/badges/master/coverage.svg)](https://gitlab.com/protocod/biolib/-/commits/master)
[![documentation](http://gitlab.com/protocod/biolib/-/jobs/artifacts/master/raw/documentation.svg?job=create_badge_svg)](https://protocod.gitlab.io/biolib/)

## Description
Biotools-py (not called biolib anymore to avoid confusion with the existing famous biolib) is a small lib which provide bioinformatics stuff created by a student. The code is an homework to the University of Lille in master degree in Bioinformatics for OMICS data.

## Features:
- DNA complementary
- Parser
    - fasta
    - score config file
- Sequence alignment
    - Global aligner using needleman wunsch algorithm
    - Local aligner using smith waterman algorithm

## Installation

There is no way to install the library using pip for the moment. So you have to use git to get the latest stable version on master branch.

Using ssh
```shell
git clone git@gitlab.com:protocod/biolib.git
```

or using http
```shell
git clone https://gitlab.com/protocod/biolib.git
```

Then you will see the biolib folder in your git local repository

## How to use the main program ?

Biolib can be use a program or a library.

```shell
python main.py --config score.txt --seq1 seq1.fasta --seq2 seq2.fasta
```

## How to use it as a library ?

You can simply call the biolib module to import what do you need.
