from biolib.aligner.local_aligner import SmithWaterman
from biolib.cost import Cost
import argparse
from biolib.io_utils import open_stream
from biolib.config_parser import ConfigParser
from biolib.fasta_parser import FastaParser
from biolib.significance import significance


def main():
    args_parser = argparse.ArgumentParser()
    arg_options = [
        {
            'short-option': '-c',
            'option': '--config',
            'help': 'Specify the config file to get the score',
            'required': True
        },
        {
            'short-option': '-s1',
            'option': '--seq1',
            'help': 'Specify the first fasta file',
            'required': True
        },
        {
            'short-option': '-s2',
            'option': '--seq2',
            'help': 'Specify the second fasta file',
            'required': True
        },
    ]

    for arg_option in arg_options:
        args_parser.add_argument(arg_option['short-option'], arg_option['option'], help = arg_option['help'], required = arg_option['required']) 

    args = args_parser.parse_args()
    
    config_parser = ConfigParser()
    fasta_parser = FastaParser()

    score = open_stream(args.config, config_parser.parse)
    try:
        fasta_seq1 = open_stream(args.seq1, fasta_parser.parse)
        fasta_seq2 = open_stream(args.seq2, fasta_parser.parse)

        aligner = SmithWaterman(fasta_seq1, fasta_seq2, Cost(score))
        alignment_str = f'{aligner}\n'
        print(alignment_str)

        alignment_result = aligner.get_alignment_result()

        significance_result = significance(fasta_seq1, fasta_seq1, alignment_result.get_similarity_score(), 200, score)
        significance_str = f'{significance_result}'
        print(significance_str)
    
    except ValueError as err:
        print(f"/!\ {err}")


if __name__ == "__main__":
    main()